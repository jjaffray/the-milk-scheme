package main

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"
	"os"
	"the-milk-scheme/scheme"

	"github.com/bmizerany/pat"
	"github.com/coopernurse/gorp"
	"github.com/joho/godotenv"
	_ "github.com/mattn/go-sqlite3"
)

func handler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	schemes, _ := scheme.All(dbmap)
	t, _ := template.ParseFiles("templates/scheme-list.html")
	t.Execute(w, schemes)
}

func main() {
	err := godotenv.Load()
	if err != nil {
		panic("Couldn't load environment variables from .env!")
	}

	db, err := sql.Open("sqlite3", "./db.db")
	if err != nil {
		panic("Couldn't open db! Did you make it with ./make-db.sh?")
	}

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	dbmap.AddTableWithName(scheme.User{}, "users").SetKeys(true, "Id")
	dbmap.TraceOn("[gorp]", log.New(os.Stdout, "the-milk-scheme:", log.Lmicroseconds))

	m := pat.New()

	scheme.RegisterRoutes(m, dbmap)

	m.Get("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler(w, r, dbmap)
	}))

	http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	http.Handle("/", m)

	err = http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
