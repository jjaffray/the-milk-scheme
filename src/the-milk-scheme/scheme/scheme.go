package scheme

import (
	"bytes"
	"fmt"
	"os"
	"text/template"

	"github.com/coopernurse/gorp"
	"github.com/mattbaird/gochimp"
)

type Scheme struct {
	Id        int    `id`
	WhoseTurn int    `whoseTurn`
	IsEmpty   bool   `isEmpty`
	Title     string `title`
	Users     []User
}

type User struct {
	Id               int    `db:"id"`
	Name             string `db:"name"`
	PicUrl           string `db:"picUrl"`
	Email            string `db:"email"`
	PositionInScheme int    `db:"positionInScheme"`
	SchemeId         int    `db:"schemeId"`
	IsTurn           bool   `db:"-"`
}

func All(dbmap *gorp.DbMap) ([]Scheme, error) {
	var schemes []Scheme
	_, err := dbmap.Select(&schemes, "select * from schemes")
	if err != nil {
		return nil, err
	}

	return schemes, nil
}

func Get(dbmap *gorp.DbMap, schemeId int) (Scheme, error) {
	var scheme Scheme
	err := dbmap.SelectOne(&scheme, "select * from schemes where id = ?", schemeId)
	if err != nil {
		return Scheme{}, err
	}

	var users []User
	_, err = dbmap.Select(&users, "select * from users where schemeId = ?", schemeId)
	if err != nil {
		return Scheme{}, err
	}
	users[scheme.WhoseTurn].IsTurn = true
	scheme.Users = users
	return scheme, nil
}

func (u *User) SendEmail(subject string, content string) {
	apiKey := os.Getenv("MANDRILL_API_KEY")
	api, err := gochimp.NewMandrill(apiKey)
	if err != nil {
		fmt.Println("couldn't create api for mandrill")
	}

	recepient := gochimp.Recipient{
		Email: u.Email,
		Name:  u.Name,
	}

	message := gochimp.Message{
		Text:      content,
		Subject:   subject,
		To:        []gochimp.Recipient{recepient},
		FromEmail: "themilkscheme@whocouldthat.be",
	}

	_, err = api.MessageSend(message, false)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (u *User) EmailContentsForTemplate(templatePath string) (string, error) {
	t, _ := template.ParseFiles(templatePath)

	var contentBuffer bytes.Buffer
	err := t.Execute(&contentBuffer, u)
	if err != nil {
		return "", err
	}
	content := contentBuffer.String()
	return content, nil
}

func (s *Scheme) RanOut(dbmap *gorp.DbMap) {
	_, err := dbmap.Exec("update schemes set isEmpty = 1 where id=?", s.Id)

	user := s.Users[s.WhoseTurn]
	content, err := user.EmailContentsForTemplate("templates/buyit.html")
	if err != nil {
		fmt.Println(err.Error())
	}
	user.SendEmail("Buy the milk!", content)

	fmt.Println(s.Users[s.WhoseTurn])
	if err != nil {
		panic(err)
	}
}

func (s *Scheme) Purchase(dbmap *gorp.DbMap) {
	newUserIndex := (s.WhoseTurn + 1) % len(s.Users)
	_, err := dbmap.Exec("update schemes set isEmpty = 0, whoseTurn = ? where id = ?",
		newUserIndex,
		s.Id)
	user := s.Users[newUserIndex]
	content, err := user.EmailContentsForTemplate("templates/yourturn.html")
	if err != nil {
		fmt.Println(err.Error())
	}
	user.SendEmail("Your turn is coming up!", content)

	if err != nil {
		panic(err)
	}
}

func (s *Scheme) DeleteUserAt(index int, dbmap *gorp.DbMap) {
	for i := index + 1; i < len(s.Users); i++ {
		nextUser := s.Users[i]
		nextUser.PositionInScheme -= 1
		_, err := dbmap.Update(&nextUser)
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	user := s.Users[index]
	dbmap.Exec("delete from users where id=?", user.Id)
}
