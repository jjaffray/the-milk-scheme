package scheme

import (
	"fmt"
	"net/http"
	"strconv"
	"text/template"

	"github.com/bmizerany/pat"
	"github.com/coopernurse/gorp"
)

type SchemeView struct {
	CurrentUser User
	OtherUsers  []User
	IsEmpty     bool
	Id          int
}

func getScheme(r *http.Request, dbmap *gorp.DbMap) (Scheme, error) {
	schemeId, _ := strconv.Atoi(r.URL.Query().Get(":schemeId"))
	s, err := Get(dbmap, schemeId)
	if err != nil {
		return Scheme{}, err
	}

	return s, nil
}

func viewSchemeHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	otherUsers := make([]User, 0, len(s.Users))

	for i := (s.WhoseTurn + 1) % len(s.Users); i != s.WhoseTurn; i = (i + 1) % len(s.Users) {
		otherUsers = append(otherUsers, s.Users[i])
	}

	schemeView := SchemeView{
		CurrentUser: s.Users[s.WhoseTurn],
		OtherUsers:  otherUsers,
		IsEmpty:     s.IsEmpty,
		Id:          s.Id,
	}

	t, _ := template.ParseFiles("templates/base.html", "templates/scheme.html")
	t.ExecuteTemplate(w, "template", schemeView)
}

func editSchemeHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	t, _ := template.ParseFiles("templates/base.html", "templates/edit_scheme.html")
	t.ExecuteTemplate(w, "template", s)
}

func schemeRanOutHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	s.RanOut(dbmap)

	http.Redirect(w, r, "/scheme/"+strconv.Itoa(int(s.Id)), http.StatusSeeOther)
}

func schemePurchaseHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	s.Purchase(dbmap)

	http.Redirect(w, r, "/scheme/"+strconv.Itoa(int(s.Id)), http.StatusSeeOther)
}

func editUserHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	userPosition, _ := strconv.Atoi(r.URL.Query().Get(":userId"))

	t, _ := template.ParseFiles("templates/base.html", "templates/edit_user.html")
	t.ExecuteTemplate(w, "template", s.Users[userPosition])
}

func updateUserHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	name := r.FormValue("name")
	email := r.FormValue("email")
	picUrl := r.FormValue("picurl")

	userPosition, _ := strconv.Atoi(r.URL.Query().Get(":userId"))

	userId := s.Users[userPosition].Id
	dbmap.Exec("update users set name=?, email=?, picUrl=? where id=?", name, email, picUrl, userId)

	editUserHandler(w, r, dbmap)
}

func newUserHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}

	_, err = dbmap.Exec("insert into users (schemeId, name, email, picUrl, positionInScheme) values (?, \"\", \"\", \"nobody.gif\", ?)", s.Id, len(s.Users))
	if err != nil {
		fmt.Println(err)
	}

	http.Redirect(w, r, "/scheme/"+strconv.Itoa(int(s.Id))+"/edit", http.StatusSeeOther)
}

func deleteUserHandler(w http.ResponseWriter, r *http.Request, dbmap *gorp.DbMap) {
	s, err := getScheme(r, dbmap)
	if err != nil {
		panic(err.Error())
	}
	userPosition, _ := strconv.Atoi(r.URL.Query().Get(":userId"))
	s.DeleteUserAt(userPosition, dbmap)
	http.Redirect(w, r, "/scheme/"+strconv.Itoa(int(s.Id))+"/edit", http.StatusSeeOther)
}

// Alias type
type Handler func(http.ResponseWriter, *http.Request, *gorp.DbMap)

// Convenience functions to simplify passing around the DbMap
func registerMethod(h func(string, http.Handler), pattern string, dbmap *gorp.DbMap, handler Handler) {
	h(pattern, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler(w, r, dbmap)
	}))
}

func registerGet(m *pat.PatternServeMux, pattern string, dbmap *gorp.DbMap, handler Handler) {
	registerMethod(m.Get, pattern, dbmap, handler)
}

func registerPost(m *pat.PatternServeMux, pattern string, dbmap *gorp.DbMap, handler Handler) {
	registerMethod(m.Post, pattern, dbmap, handler)
}

func RegisterRoutes(m *pat.PatternServeMux, dbmap *gorp.DbMap) {
	registerGet(m, "/scheme/:schemeId", dbmap, viewSchemeHandler)
	registerGet(m, "/scheme/:schemeId/edit", dbmap, editSchemeHandler)

	registerPost(m, "/scheme/:schemeId/ranout", dbmap, schemeRanOutHandler)
	registerPost(m, "/scheme/:schemeId/purchase", dbmap, schemePurchaseHandler)

	// User stuff
	registerGet(m, "/scheme/:schemeId/users/:userId/edit", dbmap, editUserHandler)
	registerPost(m, "/scheme/:schemeId/users/:userId/update", dbmap, updateUserHandler)

	// these should be POSTs...
	registerGet(m, "/scheme/:schemeId/users/new", dbmap, newUserHandler)
	registerGet(m, "/scheme/:schemeId/users/:userId/delete", dbmap, deleteUserHandler)
}
