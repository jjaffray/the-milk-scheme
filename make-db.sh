#!/bin/bash

rm -f db.db
sqlite3 db.db < schema.sqlite

# Create a seed entry
sqlite3 db.db <<EOF
INSERT INTO schemes (id, whoseTurn, title, isEmpty) VALUES (NULL, 1, "352 UDC", 0);
INSERT INTO users (id, picUrl, name, email, positionInScheme, schemeId)
  VALUES (NULL, "justin.png", "Justin", "justin.jaffray@gmail.com", 0, 1);
INSERT INTO users (id, picUrl, name, email, positionInScheme, schemeId)
  VALUES (NULL, "omar.png", "Omar", "justin.jaffray+omar@gmail.com", 1, 1);
INSERT INTO users (id, picUrl, name, email, positionInScheme, schemeId)
  VALUES (NULL, "spencer.png", "Spencer","justin.jaffray+spencer@gmail.com", 2, 1);
EOF
